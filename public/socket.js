var socket = io.connect();

socket.on('connect', function () {
$('#splash').addClass('connected');

});

socket.on('world', function (players, food){
    $('#nicknames').empty().append($('<span>Online: </span>'));
        for (var i in players) {
          $('#nicknames').append($('<b>').text(players[i].nickname + ":" + players[i].score));
        }
    draw(players, $('#nick').val(), food);
});

socket.on('announcement', function (msg) {
$('#lines').append($('<p>').append($('<em>').text(msg)));
});

socket.on('nicknames', function (nicknames, players, food) {
    $('#nicknames').empty().append($('<span>Online: </span>'));
    for (var i in nicknames) {
      $('#nicknames').append($('<b>').text(nicknames[i].nickname));
    }
    draw(players, $('#nick').val(), food);
});

socket.on('user message', message);
socket.on('reconnect', function () {
$('#lines').remove();
message('System', 'Reconnected to the server');
});

socket.on('reconnecting', function () {
message('System', 'Attempting to re-connect to the server');
});

socket.on('error', function (e) {
message('System', e ? e : 'A unknown error occurred');
});

function message (from, msg) {
$('#lines').append($('<p>').append($('<b>').text(from), msg));
}
