var Food = function (image, food) {
// food
    return new Kinetic.Shape(function () {
        var context = this.getContext();
        var width = 50, height = 50, x = 500 + (50 * food.x), y = 300 + (50 * food.y);
        context.translate(x, y);
        context.rotate(0);
        context.drawImage(image, -width / 2, -height / 2, width, height);
        context.rotate(0);
        context.translate(-x, -y);
        });

};